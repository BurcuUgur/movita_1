package actions;

import controls.ButtonControl;

public class ButtonActions {


    public static void click(String label, int n) {
        if (n<1) n=1;
        ButtonControl.fromLabel(label,n).click();
    }

    public static void click(String label) {
        click(label, "button");
    }

    public static void click(String label, String type) {
        switch (type.toLowerCase().trim()) {
            case "locator":
                ButtonControl.fromLocator(label)
                        .click();
                break;
            default:
                ButtonControl.fromLabel(label)
                        .click();

        }
    }

}

package actions;

import controls.ButtonControl;
import controls.TextfieldControl;
import utils.TestConfig;
import utils.TestConfiguration;
import webdriver.Driver;

public class ApplicationActions {

    public void gotoHomeSite() {
        TestConfig config = TestConfiguration.instance().getTestConfiguration();
        String url = config.getApplication().getUrl();
        Driver.getDriver().get(url);
    }

    public void login() {
        TestConfig config = TestConfiguration.instance().getTestConfiguration();
        String username = config.getApplication().getUsername();
        String password = config.getApplication().getPassword();
        login(username, password);
    }

    private void login(String username, String password) {
        TextfieldControl.fromLabel("Username").setText(username);
        TextfieldControl.fromLabel("Password").setText(password);
        ButtonControl.fromLabel("Login").click();

    }
}
